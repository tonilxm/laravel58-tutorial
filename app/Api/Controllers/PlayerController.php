<?php

namespace App\Api\Controllers;

use App\Api\Transformers\PlayerTransformer;
use App\Api\Transformers\UserTransformer;
use Illuminate\Http\Request;
use App\Services\PlayerService;
use Dingo\Api\Routing\Helpers;

class PlayerController extends BaseController
{
    use Helpers;

    private $playerService;

    function __construct(PlayerService $playerService)
    {
        $this->playerService = $playerService;
    }


    public function insert(Request $request)
    {
        $name = $request->input('name');
        $age = $request->input('age');

        $player = $this->playerService->insert($name, $age);

        return $this->response->item($player, new PlayerTransformer);
    }
}
