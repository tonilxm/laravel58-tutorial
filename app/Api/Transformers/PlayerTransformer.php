<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;

class PlayerTransformer extends TransformerAbstract
{
    public function transform($player)
    {
        return [
            'id'    => $player->id,
            'name'  => $player->name,
            'age'   => $player->age
        ];
    }
}
