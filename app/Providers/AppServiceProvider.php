<?php

namespace App\Providers;

use App\Http\Services\UserService;
use App\Http\Services\Impl\UserServiceImpl;
use App\Http\Services\Impl\UserGroupServiceImpl;
use App\Http\Services\UserGroupService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserService::class, UserServiceImpl::class);
        $this->app->bind(UserGroupService::class, UserGroupServiceImpl::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
