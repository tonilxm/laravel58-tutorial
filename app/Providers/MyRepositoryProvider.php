<?php

namespace App\Providers;

use App\Repositories\PlayerRepository;
use App\Repositories\PlayerRepositoryImpl;
use Illuminate\Support\ServiceProvider;

class MyRepositoryProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PlayerRepository::class, PlayerRepositoryImpl::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
