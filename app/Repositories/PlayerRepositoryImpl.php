<?php

namespace App\Repositories;

use App\Models\Player;

class PlayerRepositoryImpl implements PlayerRepository
{
    public function insert($name, $age)
    {
        $data = Player::create(['name' => $name, 'age' => $age]);
        return $data->id;
    }
}
