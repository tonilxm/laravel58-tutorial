<?php

namespace App\Services;

use App\Models\Player;
use App\Repositories\PlayerRepository;

class PlayerServiceImpl implements PlayerService
{
    public $playerRepository;

    function __construct(PlayerRepository $playerRepository)
    {
        $this->playerRepository = $playerRepository;
    }

    public function insert($name, $age)
    {
        // .... Put Logic Here
        $id = $this->playerRepository->insert($name, $age);

        $player = new Player;
        $player->id = $id;
        $player->name = $name;
        $player->age = $age;

        return $player;
    }
}
