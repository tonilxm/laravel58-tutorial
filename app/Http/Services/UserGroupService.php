<?php

namespace App\Http\Services;

interface UserGroupService
{
    function saveUserGroup(string $groupName);
}
