<?php

namespace App\Http\Services\Impl;

use App\Http\Services\UserGroupService;

class UserGroupServiceImpl implements UserGroupService
{
    public function saveUserGroup(string $groupName)
    {
        return 'Saving UserGroup : ' . $groupName;
    }
}
