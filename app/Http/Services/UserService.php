<?php

namespace App\Http\Services;

interface UserService
{
    function saveUser(string $name, string $age);
}
