<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Services\Impl\UserServiceImpl2;
use App\Http\Services\UserService;
use App\Http\Services\UserGroupService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $userService;
    private $userGroupService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(UserService $userService, UserGroupService $userGroupService)
    {
        $this->userService = $userService;
        $this->userGroupService = $userGroupService;
    }

    public function saveUser(Request $request)
    {
        $name = $request->input('name');
        $age = $request->input('age');

        return $this->userService->saveUser($name, $age);
    }

    public function saveUserGroup(Request $request)
    {
        $userGroupName = $request->input('userGroup');

        return $this->userGroupService->saveUserGroup($userGroupName);
    }
}
